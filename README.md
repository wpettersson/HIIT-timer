# HIIT Timer
A simple timer for high intensity trainings

This software was originally written by Lorenzo Chiovini, and has since been forked by William Pettersson to add some new features.
So far, the new features include:

* preference option to disable beeps
* updated GUI with large circular progress tracker
* updated GUI to work in rotated mode
* allow disabling of notifications
* countdown beeps are now alarms, not media
* running training plans are shown as such in main menu

Further changes targeted include:

* Show the user what round is coming next
* Add text-to-speech support for rounds
* Implement some sort of test suite

## Installation and usage

Please note that for the moment, this particular fork of this package is not
available in any online store. I aim to upload to F-Droid at some point (I might
need to take over the existing app, unsure at the moment),but I imagine that
Google will never support GPLv3 software in the Google Play store, so I doubt it
will ever be available there.



    Copyright (C) 2015 Lorenzo Chiovini
              (C) 2020 William Pettersson
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Database-related code was generated using greenDAO ( http://greendao-orm.com/ )

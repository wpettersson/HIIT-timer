package se.ewpettersson.hiittimer.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import se.ewpettersson.hiittimer.Constants;
import se.ewpettersson.hiittimer.CurrentTraining;
import se.ewpettersson.hiittimer.HIITTimerApplication;
import se.ewpettersson.hiittimer.database.generated.DaoSession;
import se.ewpettersson.hiittimer.database.generated.Round;
import se.ewpettersson.hiittimer.database.generated.TrainingPlan;
import se.ewpettersson.hiittimer.enums.TrainingAction;
import se.ewpettersson.hiittimer.R;

public final class CurrentTrainingService extends Service {

	private final static IntentFilter INTENT_FILTER = new IntentFilter(Constants.ON_TICK_FINISHED_BROADCAST_ACTION);
	private static TrainingPlan trainingPlan;
	private Long trainingPlanId;
	private long currentRoundId;
	private String currentRoundName;
	private long totalRounds;
	private OnTickFinishedReceiver onTickFinishedReceiver;
	private Intent currentRoundServiceIntent;
	private static volatile boolean isRunning = false;
	private PhoneStateListener phoneStateListener;
	private final String CHANNEL_ID = "hiit-timer";
	private SharedPreferences sharedPreferences;

	private class OnTickFinishedReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			performRound();
		}

	}

	private DaoSession getDaoSession() {
		return ((HIITTimerApplication) getApplication()).getDaoSession();
	}

	private void stopNotification() {
		final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null)
		    notificationManager.cancel(R.id.notification_id);
	}

	private Notification trainingNotification() {
		final Intent intent = new Intent(this, CurrentTraining.class);
		intent.putExtra(Constants.TRAINING_ID, trainingPlanId);
		final Notification notification;
		Resources res = getResources();
		String text = res.getString(R.string.notification_text, trainingPlan.getName(), currentRoundId, totalRounds, getCurrentRoundName());

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			notification = new NotificationCompat.Builder(this, CHANNEL_ID)
					.setContentTitle(getString(R.string.notification_title))
					.setSmallIcon(R.drawable.ic_notif)
					.setContentText(text)
					.setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)).build();
		} else {
			notification = new NotificationCompat.Builder(this)
					.setContentTitle(getString(R.string.notification_title))
					.setSmallIcon(R.drawable.ic_notif)
					.setContentText(text)
					.setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)).build();
		}
		notification.flags = Notification.FLAG_ONGOING_EVENT;

		return notification;
	}

	private void showNotification() {
		final NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null)
		    notificationManager.notify(R.id.notification_id, trainingNotification());

	}

	private void performRound() {
		if (++currentRoundId <= totalRounds) {
			currentRoundServiceIntent = new Intent(this, CurrentRoundService.class);
			final Bundle roundData = setupRoundDataBundle();

			currentRoundServiceIntent.putExtras(roundData);

			startService(currentRoundServiceIntent);

			if (sharedPreferences.getBoolean(getResources().getString(R.string.preferences_notifications_onoff_key), true)) {
				showNotification();
			}
		} else {
			updateCurrentRoundActivityWhenTrainingIsFinished();
			stopSelf();
		}

	}

	private Bundle setupRoundDataBundle() {
		final Bundle roundData = new Bundle();
		final Round currentRound = trainingPlan.getRound((int) currentRoundId - 1);
		String nextRoundName = "";
		if ( currentRoundId < totalRounds) {
			nextRoundName = trainingPlan.getRound((int) currentRoundId).getRoundName();
		}

		roundData.putInt(Constants.ROUND_DURATION, currentRound.getWorkInSeconds());
		roundData.putInt(Constants.ROUND_RECOVER_TIME, currentRound.getRestInSeconds());
		roundData.putLong(Constants.ROUND_ID, currentRoundId);
		roundData.putString(Constants.ROUND_NAME, currentRound.getRoundName());
		setCurrentRoundName(currentRound.getRoundName());
		roundData.putString(Constants.NEXT_ROUND_NAME, nextRoundName);
		roundData.putInt(Constants.ROUND_REPEAT, currentRound.getRoundRepeat());
		roundData.putLong(Constants.TOTAL_ROUNDS, totalRounds);
		roundData.putInt(Constants.PRE_TRAINING_COUNTDOWN, trainingPlan.getGetReadyTimeInSeconds());
		return roundData;
	}

	private void stopCurrentRoundService() {
		if (currentRoundServiceIntent != null) {
			stopService(currentRoundServiceIntent);
		}

	}

	private void updateCurrentRoundActivityWhenTrainingIsTerminatedByACall() {

		final Bundle onTickIntentData = new Bundle();
		onTickIntentData.putLong(Constants.ROUND_SECONDS_LEFT, 0);
		onTickIntentData.putLong(Constants.ROUND_ID, currentRoundId);
		onTickIntentData.putString(Constants.ROUND_NAME, "end of training");
		onTickIntentData.putString(Constants.NEXT_ROUND_NAME, "");
		onTickIntentData.putInt(Constants.ROUND_REPEAT, 0);
		onTickIntentData.putLong(Constants.TOTAL_ROUNDS, totalRounds);
		onTickIntentData.putSerializable(Constants.TRAINING_ACTION, TrainingAction.TERMINATED_BY_CALL);

		updateCurrentRoundActivity(onTickIntentData);
	}

	private void updateCurrentRoundActivityWhenTrainingIsFinished() {

		final Bundle onTickIntentData = new Bundle();
		onTickIntentData.putLong(Constants.ROUND_SECONDS_LEFT, 0);
		onTickIntentData.putLong(Constants.ROUND_ID, totalRounds);
		onTickIntentData.putString(Constants.ROUND_NAME, "end of training");
		onTickIntentData.putString(Constants.NEXT_ROUND_NAME, "");
		onTickIntentData.putInt(Constants.ROUND_REPEAT, 0);
		onTickIntentData.putLong(Constants.TOTAL_ROUNDS, totalRounds);
		onTickIntentData.putSerializable(Constants.TRAINING_ACTION, TrainingAction.FINISHED);

		updateCurrentRoundActivity(onTickIntentData);
	}

	private void updateCurrentRoundActivity(Bundle onTickIntentData) {
		final Intent onTickIntent = new Intent(Constants.ON_TICK_BROADCAST_ACTION);

		onTickIntent.putExtras(onTickIntentData);

		final LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

		localBroadcastManager.sendBroadcast(onTickIntent);
	}

	private void registerPhoneStateListener() {
		phoneStateListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
				case TelephonyManager.CALL_STATE_OFFHOOK:
					stopCurrentRoundService();
					updateCurrentRoundActivityWhenTrainingIsTerminatedByACall();
					CurrentTrainingService.this.stopSelf();
					break;

				default:
					break;
				}

			}

		};

		listenToPhoneState(PhoneStateListener.LISTEN_CALL_STATE);
	}

	private void listenToPhoneState(int phoneState) {
		final TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(
				Context.TELEPHONY_SERVICE);
        if (telephonyManager != null)
		    telephonyManager.listen(phoneStateListener, phoneState);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	@TargetApi(android.os.Build.VERSION_CODES.O)
	public void onCreate() {
		super.onCreate();

		int NOTIFICATION_ID = 42734;
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		if ((notificationManager != null) && (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)) {
			NotificationChannel mChannel;
			CharSequence name = "hiit-timer";
			String Description = "Round updates for HIIT timer";
			int importance = NotificationManager.IMPORTANCE_HIGH;
			mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
			mChannel.setDescription(Description);
			notificationManager.createNotificationChannel(mChannel);
		}
		 sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
	}

	private boolean havePhoneState() {
		if (Build.VERSION.SDK_INT <= 32) {
			return true;
		}
		return ContextCompat.checkSelfPermission(this.getBaseContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		isRunning = true;

		if (havePhoneState()) {
			registerPhoneStateListener();
		}

		trainingPlanId = intent.getExtras().getLong(Constants.TRAINING_ID);

		trainingPlan = getDaoSession().getTrainingPlanDao().load(trainingPlanId);

		currentRoundId = 0;
		setCurrentRoundName("Preparation");

		onTickFinishedReceiver = new OnTickFinishedReceiver();

		LocalBroadcastManager.getInstance(this).registerReceiver(onTickFinishedReceiver, INTENT_FILTER);

		totalRounds = trainingPlan.getRounds().size();
		if (sharedPreferences.getBoolean(getResources().getString(R.string.preferences_notifications_onoff_key), true)) {
			showNotification();
		}

		performRound();

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (havePhoneState()) {
			listenToPhoneState(PhoneStateListener.LISTEN_NONE);

			final TelephonyManager telephonyManager = (TelephonyManager) getApplicationContext().getSystemService(
					Context.TELEPHONY_SERVICE);
			if (telephonyManager != null)
				telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		}

		stopCurrentRoundService();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(onTickFinishedReceiver);

		stopNotification();

		isRunning = false;
	}

	public TrainingPlan getTrainingPlan() {
		return trainingPlan;
	}

	public void setTrainingPlan(TrainingPlan trainingPlan) {
		this.trainingPlan = trainingPlan;
	}

	public Long getTrainingPlanId() {
		return trainingPlanId;
	}

	public void setTrainingPlanId(Long trainingPlanId) {
		this.trainingPlanId = trainingPlanId;
	}

	public long getCurrentRoundId() {
		return currentRoundId;
	}

	public void setCurrentRoundId(long currentRoundId) {
		this.currentRoundId = currentRoundId;
	}

	public String getCurrentRoundName() {
		return currentRoundName;
	}

	private void setCurrentRoundName(String currentRoundName) {
		this.currentRoundName = currentRoundName;
	}

	public static boolean isRunning() {
		return isRunning;
	}

	public static boolean isRunning(TrainingPlan otherTrainingPlan) {
		if (!isRunning) {
			return false;
		}
		if (trainingPlan == otherTrainingPlan) {
			return true;
		}
		return false;
	}

}

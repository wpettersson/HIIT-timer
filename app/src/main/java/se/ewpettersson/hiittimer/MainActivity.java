/*
 * 
 * HIIT Timer - A simple timer for high intensity trainings
 Copyright (C) 2015 Lorenzo Chiovini

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package se.ewpettersson.hiittimer;

import se.ewpettersson.hiittimer.services.CurrentTrainingService;
import se.ewpettersson.hiittimer.R;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.FragmentActivity;

public final class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container,
					PlaceholderFragment.class,null).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		stopService(new Intent(this, CurrentTrainingService.class));
	}

	public static class PlaceholderFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_main, container, false);
		}

		@Override
		public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			if (Build.VERSION.SDK_INT > 32) {
				if (ContextCompat.checkSelfPermission(getActivity(),
						Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
					// You can use the API that requires the permission.
					// performAction(...);
					// } else if (shouldShowRequestPermissionRationale(...)){
					// In an educational UI, explain to the user why your app requires this
					// permission for a specific feature to behave as expected, and what
					// features are disabled if it's declined. In this UI, include a
					// "cancel" or "no thanks" button that lets the user continue
					// using your app without granting the permission.
					//howInContextUI(...);
				} else {
					// You can directly ask for the permission.
					// The registered ActivityResultCallback gets the result of this request.
					requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS);
				}
			}
		}

		// Register the permissions callback, which handles the user's response to the
		// system permissions dialog. Save the return value, an instance of
		// ActivityResultLauncher, as an instance variable.
		private final ActivityResultLauncher<String> requestPermissionLauncher =
				registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
					if (isGranted) {
						// Permission is granted. Continue the action or workflow in your
						// app.
					} else {
						// Explain to the user that the feature is unavailable because the
						// feature requires a permission that the user has denied. At the
						// same time, respect the user's decision. Don't link to system
						// settings in an effort to convince the user to change their
						// decision.
					}
				});

	}

	public void trainingPlans(View view) {
		final Intent intent = new Intent(this, TrainingPlans.class);

		startActivity(intent);

	}

	public void preferences(View view) {
		final Intent intent = new Intent(this, PreferencesActivity.class);

		startActivity(intent);
	}

}
